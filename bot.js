//Basic Setup

const Moralis = require("moralis").default;
const nodemailer = require("nodemailer");
const fs = require('fs');

const { ethers } = require("ethers")
require("dotenv").config()
const RouterABI = require("@uniswap/v2-periphery/build/UniswapV2Router02.json").abi
const ERC20ABI = require("@openzeppelin/contracts/build/contracts/ERC20.json").abi

//Private info stored in .ENV file
const privateKey = process.env.PRIVATE_KEY
const rpc = process.env.RPC_URL
const api = process.env.MORALIS_API

//Token and Router Addressess
const USD = "0xc2132D05D31c914a87C6611C10748AEb04B58e8F"
const WrappedMATIC = "0x0d500B1d8E8eF31E21C99d1Db9A6444d3ADf1270"
const QuickSwapRouter = "0xa5E0829CaCEd8fFDD4De3c43696c57F7D7A678ff"

let provider
let wallet
let lastBuyPrice = 0.862192167296667
let lastSellPrice = 0.90
let previousPrice = 0.862192167296667
let lastPriceCheck = 0.862192167296667
let fundsInUSDT = false
let profitMarginReached = false   //Used to check if a certain profit has been reached yet or not
let profitMarginPrice = lastBuyPrice + (lastBuyPrice * .03) 
let buyInPrice = lastSellPrice - (lastSellPrice * .05)
let buyInMarginReached = false    //Used to check if a certain buy in price has been reached yet or not

async function checkPrice(){     //Waits 1 hr then checks price and buys/sells if needed
    try {
        const chain = 137;
    
        const address = WrappedMATIC;
    
        const response = await Moralis.EvmApi.token.getTokenPrice({
            address,
            chain,
        });
        
        console.log(response.result);
        previousPrice = lastPriceCheck
        lastPriceCheck = response.result.usdPrice
        console.log("Last price check was equal to " + lastPriceCheck)

    } catch (e) {
         console.error(e);
     }
     //Set profitMarginReached to true if funds are not in USDT and current price is 3% greater than last buy price
     if (profitMarginReached == false && lastPriceCheck >= profitMarginPrice && !fundsInUSDT) {
         profitMarginReached = true
     }
     //Sell if new price check is 2% less than last price check and profit margin has been reached
     //This allows price to continue to climb but sell off if price looks like it is dropping again
     else if(profitMarginReached == true && lastPriceCheck <= previousPrice - (previousPrice * .02) && !fundsInUSDT){
         await sell()
     }
     //Failsafe to ensure we get at least 2% profit on a sell
     else if(profitMarginReached == true && lastPriceCheck <= profitMarginPrice - (profitMarginPrice * .01) && lastPriceCheck > lastBuyPrice + (lastBuyPrice * .01) && !fundsInUSDT){
        await sell()
    }
     //Set buyInMarginReached to true if funds are in USDT and price has fallen 5% since last sell
     else if (buyInMarginReached == false && lastPriceCheck <= buyInPrice && fundsInUSDT) {
        buyInMarginReached = true
     }
     //Buy if new price check is 2% higher than last price check and buy in margin has been reached
     //This allows price to continue to drop but buys if price looks to be on the rise again
     else if (buyInMarginReached == true && lastPriceCheck >= previousPrice + (previousPrice * .02) && fundsInUSDT){
         await buy()
     }
     //Failsafe to ensure we get at least 3% profit when buying
     else if (buyInMarginReached == true && lastPriceCheck >= buyInPrice + (buyInPrice * .02 ) && lastPriceCheck < lastSellPrice - (lastSellPrice * .01) && fundsInUSDT){
        await buy()
    }
     
     const data = {
        funds_Are_In_USDT: fundsInUSDT,
	ready_To_Sell: profitMarginReached,
	ready_To_Buy: buyInMarginReached,
        last_Price_Check: lastPriceCheck,
        last_Buy_Price: lastBuyPrice,
        last_Sell_Price: lastSellPrice,
        last_Percent_Change: ((lastPriceCheck - previousPrice) / previousPrice) * 100
     };


     // Convert the variables to a JSON string
     const jsonData = JSON.stringify(data, null, 2); // the third argument is the number of spaces for indentation

     // Specify the file path
     const filePath = 'output.txt';

     // Write to the file
     fs.writeFile(filePath, jsonData, (err) => {
       if (err) {
         console.error('Error writing to file:', err);
       } else {
         console.log('Variables written to', filePath);
       }
     });

     await wait()
     checkPrice()
}

//Build copies of contracts and wallet to interact with
async function build() {
    provider = new ethers.providers.JsonRpcProvider(rpc)
    wallet = new ethers.Wallet(privateKey, provider)
    uniswapV2Router = new ethers.Contract(QuickSwapRouter, RouterABI, wallet)
    USDT = new ethers.Contract(USD, ERC20ABI, wallet)
    WMATIC = new ethers.Contract(WrappedMATIC, ERC20ABI, wallet)
}

async function buy(){
    myUSDTBalance = await USDT.balanceOf(wallet.address)
    let gasUsageEstimation1 = await USDT.estimateGas.approve(QuickSwapRouter, myUSDTBalance);

    let gasPriceEstimation1 = (Math.floor(parseInt((await provider.getGasPrice()).toString()) * 1.1)).toString()

    let options1 = {
        gasPrice: gasPriceEstimation1,
        gasLimit: gasUsageEstimation1
    }

    let tx1 = await USDT.approve(QuickSwapRouter, myUSDTBalance, options1)
    await tx1.wait()

    let blockNumber = await provider.getBlockNumber()

    let timestamp = (await provider.getBlock(blockNumber)).timestamp;

     let gasUsageEstimation = await uniswapV2Router.estimateGas.swapExactTokensForETH(myUSDTBalance, 0, [USD, WrappedMATIC], wallet.address, timestamp + 60);

    let gasPriceEstimation = (Math.floor(parseInt((await provider.getGasPrice()).toString()) * 1.1)).toString()

    let options = {
        gasPrice: gasPriceEstimation,
        gasLimit: gasUsageEstimation
    }

    let tx2 = await uniswapV2Router.swapExactTokensForETH(myUSDTBalance, 0, [USD, WrappedMATIC], wallet.address, timestamp + 60, options)
    await tx2.wait()

    buyInMarginReached = false
         fundsInUSDT = false
         lastBuyPrice = lastPriceCheck

         const transporter = nodemailer.createTransport({
            service : process.env.EMAIL_SERVICE,
            auth : {
                user : process.env.EMAIL_ADDRESS,
                pass : process.env.EMAIL_PASS
            }
        })
        
        const mailoptions = {
            from : process.env.EMAIL_ADDRESS, 
            to: process.env.EMAIL_ADDRESS, 
            subject: "Swap Made", 
            text: "You bought MATIC with your USDT."
        }

        transporter.sendMail(mailoptions, (error, info) =>{
            if(error) console.log(error)
            else console.log(info)
        })
}

async function sell(){
    myMaticBalance = await provider.getBalance(wallet.address) - 5000000000000000000

    let blockNumber = await provider.getBlockNumber()

    let timestamp = (await provider.getBlock(blockNumber)).timestamp;

     let gasUsageEstimation = await uniswapV2Router.estimateGas.swapExactETHForTokens(
         0, [WrappedMATIC, USD],
         wallet.address,
         timestamp + 60,
         {value: myMaticBalance.toString()}
     );

    let gasPriceEstimation = (Math.floor(parseInt((await provider.getGasPrice()).toString()) * 1.1)).toString()
    let options = {
        value: myMaticBalance.toString(),
        gasPrice: gasPriceEstimation,
        gasLimit: gasUsageEstimation
    }

    await uniswapV2Router.swapExactETHForTokens(0, [WrappedMATIC, USD], wallet.address, timestamp + 60, options)

    profitMarginReached = false
    fundsInUSDT = true
    lastSellPrice = lastPriceCheck

    const transporter = nodemailer.createTransport({
       service : process.env.EMAIL_SERVICE,
       auth : {
           user : process.env.EMAIL_ADDRESS,
           pass : process.env.EMAIL_PASS
       }
   })
   
   const mailoptions = {
       from : process.env.EMAIL_ADDRESS, 
       to: process.env.EMAIL_ADDRESS, 
       subject: "Swap Made", 
       text: "You sold your MATIC for USDT."
   }

   transporter.sendMail(mailoptions, (error, info) =>{
       if(error) console.log(error)
       else console.log(info)
   })

}


async function begin() {
    await Moralis.start({
        apiKey: api,
        // ...and any other configuration
    });

    build()
    checkPrice()
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function wait() {
    await sleep(1000 * 60 * 60)
}

begin()
